groups = {
  'cube': ['cube-ui', 'bazel-cube-ui-builder'],
  'calculator': ['calculator-ui', 'bazel-calculator-ui-builder'],
  'api': ['api', 'bazel-api-builder'],
}

config.define_string_list("to-run", args=True)
cfg = config.parse()
resources = []
for arg in cfg.get('to-run', []):
  if arg in groups:
    resources += groups[arg]
  else:
    # also support specifying individual services instead of groups, e.g. `tilt up a b d`
    resources.append(arg)

config.set_enabled_resources(resources)

BAZEL_RUN_CMD = "bazel run %s %s"

BAZEL_SOURCES_CMD = """
  bazel query 'filter("^//", kind("source file", deps(set(%s))))' --order_output=no
  """.strip()

BAZEL_BUILDFILES_CMD = """
  bazel query 'filter("^//", buildfiles(deps(set(%s))))' --order_output=no
  """.strip()

def bazel_labels_to_files(labels):
  files = {}
  for l in labels:
    path = bazel_label_to_path(l)
    if path:
        files[path] = path
    else:
        files[path] = None
  return files.keys()

def bazel_label_to_path(label):
    if label.startswith("//external/") or label.startswith("//external:"):
        return ''
    elif label.startswith("//"):
      label = label[2:]

    path = label.replace(":", "/")
    if path.startswith("/"):
      path = path[1:]

    return path

def watch_labels(labels):
  watched_files = []
  for l in labels:
    if l.startswith("@"):
      continue
    elif l.startswith("//external/") or l.startswith("//external:"):
      continue
    elif l.startswith("//"):
      l = l[2:]

    path = l.replace(":", "/")
    if path.startswith("/"):
      path = path[1:]

    watch_file(path)
    watched_files.append(path)

  return watched_files

def bazel_k8s(target):
  build_deps = str(local(BAZEL_BUILDFILES_CMD % target)).splitlines()
  source_deps = str(local(BAZEL_SOURCES_CMD % target)).splitlines()
  watch_labels(build_deps)
  watch_labels(source_deps)

  return local("bazel run %s" % target)

def bazel_build(image, image_target, source_target, sync_location, options=''):
  build_deps = str(local(BAZEL_BUILDFILES_CMD % image_target)).splitlines()
  watch_labels(build_deps)
  watch_labels([source_target])

  source_deps = str(local(BAZEL_SOURCES_CMD % source_target)).splitlines()
  source_deps_files = bazel_labels_to_files(source_deps)
  output_path = bazel_label_to_path(source_target) + '/'
  bazel_output_path = './bzl-output/' + output_path
  local("mkdir -p %s" % bazel_output_path)
  local_resource(image.replace('/','-')+'-builder', cmd="bazel build %s && rsync -r ./dist/bin/%s ./bzl-output/%s " %(source_target,output_path,output_path), deps=source_deps_files)

  custom_build(
    image,
    BAZEL_RUN_CMD % (image_target, options),
    deps=[bazel_output_path],
    tag="dev_image",
    live_update=[sync(bazel_output_path, sync_location)]
  )



k8s_yaml(helm('api/chart', name='api'))
k8s_resource('api', port_forwards=3000)

k8s_yaml(helm('calculator-ui/chart', name='calculator-ui'))
k8s_resource('calculator-ui', port_forwards=8080)

k8s_yaml(helm('cube-ui/chart', name='cube-ui'))
k8s_resource('cube-ui', port_forwards=13771)



bazel_build('bazel/cube-ui', "//cube-ui:dev_image", "//cube-ui/src:webapp", "/webapp")
bazel_build('bazel/calculator-ui', "//calculator-ui:dev_image", "//calculator-ui/src:webapp", "/webapp")
bazel_build('bazel/api', "//api:dev_image", "//api/src", "/app/api/image.binary.runfiles/proofOfConcept/api/src", "-- --norun")


