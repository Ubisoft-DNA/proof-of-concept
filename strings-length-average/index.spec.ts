import { averageStringsLengths } from './index';

describe('calcStringLength', () => {

    it('should return the average length', () => {
        const result = averageStringsLengths(["hello","hello","hello","hello"]);
        expect(result).toBe("hello".length);
    });

});
