import {calcStringLength} from '@shared/string-length';

export function averageStringsLengths(values: Array<string>): number {
    const lengths: Array<number> = [];
    for (const value of values) {
        lengths.push(calcStringLength(value));
    }
    const count = lengths.length;
    let sum = lengths.reduce((a, b) => a + b, 0);
    sum /= count;

    return sum;

}
