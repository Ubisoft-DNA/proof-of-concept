import node from 'rollup-plugin-node-resolve';

module.exports = {
    treeshake: false,
    plugins: [node()]
};
