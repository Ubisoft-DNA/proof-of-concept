import {calculate} from './string-stat-calculator';

describe('calcStringLength', () => {

    it('should return the average length', () => {
        const result = calculate('hello hello hello hello');
        expect(result.avg).toBe('hello'.length);
        expect(result.med).toBe('hello'.length);
    });

});
