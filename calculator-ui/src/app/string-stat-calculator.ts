import {averageStringsLengths} from 'strings-length-average';
import {medianStringsLengths} from 'strings-length-median';

export function calculate(input: string): {avg: number; med: number} {
    const strings = input.split(' ');
    const average = averageStringsLengths(strings);
    const median =  medianStringsLengths(strings);

    return {avg: average, med: median};
}
