import {Controller, Get} from '@nestjs/common';
import {AppService} from './app.service';

@Controller()
export class AppController {
    constructor(private readonly appService: AppService) {}

  @Get()
    getPoints(): Array<Array<number>> {
        const points: Array<Array<number>> = [];
        for (let i = 0; i < 10; i++) {
            points.push([i, this.appService.getRandomPoint()]);
        }

        return points;
    }
}
