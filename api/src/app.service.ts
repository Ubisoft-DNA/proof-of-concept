import {Injectable} from '@nestjs/common';

@Injectable()
export class AppService {
    getRandomPoint(): number {
        return Math.floor(Math.random() * 10);
    }
}
