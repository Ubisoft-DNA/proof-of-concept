import * as request from 'supertest';
import {Test} from '@nestjs/testing';
import {AppModule} from '../src/app.module';
import {INestApplication} from '@nestjs/common';

describe('AppController (api.e2e)', () => {
    let app: INestApplication;

    beforeAll(async () => {
        const moduleFixture = await Test.createTestingModule({
            imports: [AppModule]
        }).compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    // eslint-disable-next-line @typescript-eslint/promise-function-async
    it('/ (GET)', () => request(app.getHttpServer())
        .get('/')
        .expect(200));
});
