import {calcStringLength} from '@shared/string-length';

export function medianStringsLengths(values: Array<string>): number {
    const lengths: Array<number> = [];
    for (const value of values) {
        lengths.push(calcStringLength(value));
    }
    lengths.sort((a, b) => a - b);
    const lowMiddle = Math.floor((lengths.length - 1) / 2);
    const highMiddle = Math.ceil((lengths.length - 1) / 2);

    return (lengths[lowMiddle] + lengths[highMiddle]) / 2;
}
