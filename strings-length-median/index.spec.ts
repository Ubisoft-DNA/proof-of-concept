import { medianStringsLengths } from './index';

describe('calcStringLength', () => {

    it('should return the median length', () => {
        const result = medianStringsLengths(["hello","hello","hello","hello"]);
        expect(result).toBe("hello".length);
    });

});
