import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TitleComponent} from '.';

describe('TestComponent', () => {
    let component: TitleComponent;
    let fixture: ComponentFixture<TitleComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TitleComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TitleComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
