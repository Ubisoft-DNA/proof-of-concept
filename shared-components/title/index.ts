import {Component, Input, NgModule} from '@angular/core';

@Component({
    selector: 'app-title',
    templateUrl: './title.component.html',
    styleUrls: ['./title.component.css']
})
export class TitleComponent {
  @Input() title: string;
}

@NgModule({
    declarations: [TitleComponent],
    exports: [TitleComponent]
})

export class TitleModule {

}