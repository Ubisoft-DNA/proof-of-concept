import {Component} from '@angular/core';
import {calculate} from './string-stat-calculator';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string = 'bazel';
  input: string = '';
  stats: {avg: number; med: number} = {avg: 0, med: 0};

  calculate(): void {
      this.stats = calculate(this.input);
  }
}

