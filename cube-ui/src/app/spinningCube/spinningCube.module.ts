import {Component, Input, NgModule} from '@angular/core';
import { spinningCubeComponent } from "./spinningCube.component";

@NgModule({
    declarations: [
        spinningCubeComponent
    ],
    exports: [spinningCubeComponent],
})
export class SpinningCubeModule { }