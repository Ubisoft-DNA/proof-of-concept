
# Installation Guide

## 1. Update Windows to version 2004, Build 19041 or higher.
- Press `Windows+R` and type `winver`

- If you have a version _below_ 1909
    - Plan ahead of time because this update will take _all day_
    - Open an SOS ticket to update to version 1909 via Software Center
    - If your update ever gets jammed (stalling for more than 30 minutes), restart your PC

- Update to version 2004 with the [Windows Update Assistant](https://go.microsoft.com/fwlink/?LinkID=799445)
    - If it fails:
        - Delete C:\windows\softwaredistribution\datastore\logs\edb.log
        - Restart your PC
        - Try again
    - If it fails with error x80070003:
        - It means your update software is malfunctionning to a point that Ubi IT need to reinstall windows installation. So contact SOS to solve it. 
        - And then, re-run Windows Update Assistant, it may takes a couple of minutes. (and you may cannot connect your PC by Citrix/RDP, ask the SOS colleague who is helping you whether he can connect to your PC. If yes, the update process goes well)

## 2. Install WSL2
- Don't proceed if you haven't updated your Windows version to 2004, Build 19041 or higher (step 1)

- If you *don't* have WSL:
    - Open PowerShell as Administrator and run `dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart`
    
- To upgrade WSL to WSL2
    - Open PowerShell as Administrator and run `dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart`
    - Restart your PC
    - [Install Ubuntu 20.04 LTS](https://aka.ms/wslubuntu2004)
    - Setup a new username and password for your new Linux distribution
    - Open PowerShell as Administrator:
        - Run `wsl --list --verbose` to check the WSL version assigned to each Linux distribution
        - Run `wsl --set-default-version 2` to set the default version to WSL2
        - Run `wsl --set-version Ubuntu 2` to set the Ubuntu to WSL2.
        (- Run `wsl --set-version <distribution name> <versionNumber>` to set the WSL version for a specific Linux distribution)
        - If you see a message about a kernel component (WSL 2 requires an update to its kernel component):
            - [Install the update](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi) for the WSL2 Linux kernel

        - If you stuck at "Error: This update only applies to machines with Windows Subsystem for Linux"
            - Just go to "Apps & features", scroll down until you see "Windows subsystem for Linux Update".
            - Uninstall it, then re-install wsl_update_x64.msi.
    - Make sure when you run `wsl -l -v`, you can see your new distribution is running with version 2. (if you dont want to mess your working setup when you are doing this POC, you can keep Ubuntu and Ubuntu20.04 together)
            
## 3. Install Docker Desktop
- Don't proceed if you don't have WSL2 (step 2)

- [Go to docker.com](https://www.docker.com/products/docker-desktop) and install the *edge* version (2.3.3.1+)
    - Installation failed: Access to the path 'Docker Desktop Installer.exe' is denied.
        - Restart your PC
        - Re-install it

- Configure your Docker Desktop:
    - In General:
        - Expose the Docker Daemon
        - Use the WSL 2 based engine
    - In Resources:
        - In File Sharing:
            - Add your working directories
        - In Proxies:
            - Set your `HTTP` proxy to `http://proxy.ubisoft.org:3128`
            - Set your `HTTPS` proxy to `http://proxy.ubisoft.org:3128`
            - Set your proxy settings bypass to `kubernetes.docker.internal`
        - *Apply and restart before proceeding*
    - In Kubernetes:
        - Enable Kubernetes
        - Apply and restart
            - You should choose `Install` kubernetes.
            - If you didnt see the dialog of install kubernetes, double check the version of your docker desktop version.(it should be 2.3.3.1+)

- In PowerShell:
    - run `kubectl config view`: in context should have docker-desktop
    - run `kubectl get pods`: No resources found in default namespace.
    - You set your Docker Desktop with Kubernetes correctly.

## 4. Setup the Linux distribution
- Make sure you can access your distribution files in the Windows file explorer (`\\wsl$\Ubuntu-20.04`)

- Open `wsl`

- Type `printenv`: check the environment setting

- Setup your proxies and proxy bypass
    - For the global environment:
        - `export http_proxy=http://proxy.ubisoft.org:3128`
        - `export https_proxy=http://proxy.ubisoft.org:3128`
        - `export no_proxy=localhost,*.ubisoft.org,kubernetes.docker.internal,127.0.0.1`
    - For `npm`:
        - `npm config set registry="https://artifactory/api/npm/npm/"`
        - `npm config set proxy="http://proxy.ubisoft.org:3128/"`
        - `npm config set strict-ssl=false`

        - If met /mnt/c/Program Files/nodejs/npm: /bin/sh^M: bad interpreter: No such file or directory:        
            - It means, if you are using new distro, you need to install npm in it.
                [Reference](https://linuxize.com/post/how-to-install-node-js-on-ubuntu-20-04/)
            - Close and re-open your wsl
    - For `apt`:
        - `sudo vim /etc/apt/apt.conf`
        - Write this line and save: `Acquire::http::Proxy "http://proxy.ubisoft.org:3128";`

- Update and install essentials
    - `sudo apt update && sudo apt upgrade`
    - `sudo apt install -y curl gnupg apt-transport-https gnupg2 build-essential checkinstall`
    - `sudo apt install -y libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev`

- Configure Docker as a non-root user
    - `sudo groupadd docker`
    - `sudo usermod -aG docker $USER`
    - Close WSL
    - Log out Windows and log back in

- Install Bazel ([official documentation](https://docs.bazel.build/versions/master/install-ubuntu.html#install-on-ubuntu))
    - `sudo apt install curl gnupg`
    - `curl -f https://bazel.build/bazel-release.pub.gpg | sudo apt-key add -`
    - `echo "deb [arch=amd64] https://storage.googleapis.com/bazel-apt stable jdk1.8" | sudo tee /etc/apt/sources.list.d/bazel.list`
    - `sudo apt update && sudo apt install bazel`
    - `sudo apt update && sudo apt full-upgrade`
    - `sudo apt install bazel-1.0.0`
    - `bazel --version`: check the bazel version

- Install Yarn ([official documentation](https://classic.yarnpkg.com/en/docs/install/#debian-stable))
    - `curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -`
    - `echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list`
    - `sudo apt install yarn`
    - `yarn --version`: check the yarn version
    - If you’re getting errors from installing yarn:
        - Ubuntu comes with cmdtest installed by default. You may want to run `sudo apt remove cmdtest` first
    - `yarn config set registry https://artifactory/api/npm/npm/`
    - `yarn config set proxy http://proxy.ubisoft.org:3128/`
    - `yarn config set strict-ssl false`

- Install kubectl ([official documentation](https://kubernetes.io/docs/tasks/tools/install-kubectl/))
    - `curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"`
    - `chmod +x ./kubectl`
    - `sudo mv ./kubectl /usr/local/bin/kubectl`
    - `kubectl version --client`
    - `sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2`
    - `curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -`
    - `echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list`
    - `sudo apt-get update`
    - `sudo apt install kubectl`
    - `kubectl config use-context docker-desktop`: Switched to context "docker-desktop".

- Install helm ([official documentation](https://helm.sh/docs/intro/install/))
    - `curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -`
    - `sudo apt-get install apt-transport-https --yes`
    - `echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list`
    - `sudo apt-get update`
    - `sudo apt-get install helm`

- Install Tilt ([official documentation](https://docs.tilt.dev/install.html))
    - Don't install Microk8s!!
    - `curl -fsSL https://raw.githubusercontent.com/tilt-dev/tilt/master/scripts/install.sh | bash`
    - `tilt --version`: check the tile version

- Install Python
    - `sudo apt update && sudo apt upgrade`
    - `sudo apt install python3 python3-pip ipython3`

## 5. Run the project
Run `tilt up`.

## FAQ

### I'm facing the error: `gpg: can't connect to the agent: IPC connect call failed`
This could be related to WSL, the best option is to google for a fix for it. By the time this file was written the fix might have changed.  

### I'm facing the error: `connection timeout on port 443`
This is most likely a proxy issue, please ensure you have the proxy environment variables set.  

### I'm facing the error: Curl: (28) Failed to connect to raw.githubusercontent.com port 443: Connection timed out
    - re-do `export HTTPS_PROXY=http://proxy.ubisoft.org:3128`
    - `git config --global https.proxy $HTTPS_PROXY`

### Yarn returns the following error: `00h00m00s 0/0: : ERROR: There are no scenarios; must have at least one.`
You probably have the wrong yarn installed (from cmdtest), uninstall it and install the proper one.  

### I'm facing the error: `Unable to connect to the server: x509: certificate signed by unknown authority` when running kubectl
The proxy is interfering with the communication with kubernetes.docker.internal, make sure to have it on your NO_PROXY list.

### I can't access my files in windows?!
Make sure your windows registry has the right entries
