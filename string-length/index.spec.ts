import { calcStringLength } from './index';

describe('calcStringLength', () => {

    it('should return the length', () => {
        const result = calcStringLength("hello");
        expect(result).toBe("hello".length);
    });

});
